# README #

### What? ###

This project was made as an assignment solution in terms of GPGPU course in LUT. The proposed bloom filter requires an RGB image to perform the operation.

### How? ###

* `python3 bloom.py -i sand.jpg`

It will produce a resulting image with the "_bloomed_gpu" prefix to the name of the original file. The image format will keep the same.
 Some images to run might be found in the folder.

Also, there are extra parameters that can be tuned. To see more information call the script with the parameter "-h".

### Who? ###

* Denis Zavialkin