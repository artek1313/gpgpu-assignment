import pycuda.driver as cuda  # import PyCuda library
from pycuda.compiler import SourceModule  # this class is needed to compile CUDA kernels
import numpy as np  # import Python module for working with numerical arrays
from time import perf_counter  # import routines for doing time measurements
import math  # import mathematical functions
from skimage import io  # import functions for image files manipulation
from jinja2 import Template  # import template engine for Python
from argparse import ArgumentParser  # import python run argument funcions
import os  # import functions to work with paths


def get_gaussian_kernel(radius, sigma):
    gaussian_kernel = np.zeros((radius, radius))
    s = 2.0 * sigma * sigma

    ker_sum = 0
    pad = int((radius - 1) / 2)

    for x in range(-pad, pad + 1):
        for y in range(-pad, pad + 1):
            gaussian_kernel[x + pad][y + pad] = (math.exp(-(x * x + y * y) / s)) / (math.pi * s)
            ker_sum += gaussian_kernel[x + pad][y + pad]

    for i in range(0, radius):
        for j in range(0, radius):
            gaussian_kernel[i][j] /= ker_sum

    return gaussian_kernel.astype('float32')


def gpu_bloom(image, threshold, kernel_radius, sigma):
    kernel_code = """
        __global__ void calc_luminance(
            float* image, // input image data, stored in row-major format
            float* output, // luminance data, stored in row-major format
            int ResX, // resolution of the input image (rows)
            int ResY, // resolution of the input image (columns)
            int* max_lum // result where max luminance stored
            )
        {
            // getting global coordinates of the thread
            int x = threadIdx.x + blockIdx.x * blockDim.x;
            int y = threadIdx.y + blockIdx.y * blockDim.y;
        
            // checking the image bounds
            if ((x >= ResX) || (y >= ResY))
                return;
        
            // index of the corresponding red pixel
            int index = x * ResY + y + 2 * (x * ResY + y);
        
            // luminance calculation
            output[index] = image[index] * 0.21 + image[index + 1] * 0.72 + image[index + 2] * 0.07;
        
            // finding max
            atomicMax(max_lum, round(output[index]));
            return;
        }
        
        __global__ void bloom_inner(
            float* image, // input image data, stored in row-major format
            float* output, // luminance data, stored in row-major format
            int ResX, // resolution of the input image (rows)
            int ResY, // resolution of the input image (columns)
            float* GKernel, //Gaussian kernel
            int max_lum // max allowed luminance
            )
        {
            // local luminance array
            extern __shared__ float luminance[];
        
            // getting global coordinates of the thread
            int x = threadIdx.x + blockIdx.x * blockDim.x;
            int y = threadIdx.y + blockIdx.y * blockDim.y;
        
            // checking the image bounds
            if ((x >= ResX) || (y >= ResY))
                return;
        
            // index inside the thread
            int local_block_pos = threadIdx.x * blockDim.y + threadIdx.y;
        
            // index of the corresponding red pixel
            int index = x * ResY + y + 2 * (x * ResY + y);
        
            // thresholding
            luminance[local_block_pos] = output[index] >= max_lum ? output[index] : 0;
            output[index] = luminance[local_block_pos];
        
            // estimating if thread is inner (does not require luminance vals outside the block)
            int center = {{ kernel_radius }} / 2;
            bool inner = (threadIdx.y < (blockDim.y - center)) && (threadIdx.x < (blockDim.x - center)) && (threadIdx.y > center - 1) && (threadIdx.x > center - 1);
        
            if (!inner) {
                return;
            }
        
            // wait till all threads in block get their luminances
            __syncthreads();
        
            // gaussian filtering coefficient
            float s = 0.0;
            for (int i = 0; i < {{ kernel_radius }} * {{ kernel_radius }}; i++) {
                int ii = i / {{ kernel_radius }};
                int jj = i % {{ kernel_radius }};
                s += luminance[local_block_pos + (ii - center) * blockDim.y + (jj - center)] * GKernel[i];
            }
        
            // add result to each of RGB pixels
            for (int i = 0; i < 3; i++) {
                image[index + i] = min(image[index + i] + s, 255.0);
            }
        }
        
        __global__ void bloom_boundary(
            float* image, // input image data, stored in row-major format
            float* output, // luminance data, stored in row-major format
            int ResX, // resolution of the input image (rows)
            int ResY, // resolution of the input image (columns)
            float* GKernel //Gaussian kernel
            )
        {
            // local luminance array
            extern __shared__ float luminance[];
        
            // getting global coordinates of the thread
            int x = threadIdx.x + blockIdx.x * blockDim.x;
            int y = threadIdx.y + blockIdx.y * blockDim.y;
        
            // checking the image bounds
            if ((x >= ResX) || (y >= ResY))
                return;
        
            // index inside the thread
            int local_block_pos = threadIdx.x * blockDim.y + threadIdx.y;
        
            // index of the corresponding red pixel
            int index = x * ResY + y + 2 * (x * ResY + y);
        
            // load luminances
            luminance[local_block_pos] = output[index];
        
            // estimating if thread is not inner (requires luminance vals outside the block)
            int center = {{ kernel_radius }} / 2;
            bool inner = (threadIdx.y < (blockDim.y - center)) && (threadIdx.x < (blockDim.x - center)) && (threadIdx.y > center - 1) && (threadIdx.x > center - 1);
        
            if (inner) {
                return;
            }
        
            // gaussian filtering coefficient
            float s = 0.0;
            int row = 0;
            int col = 0;
        
            for (int i = 0; i < {{ kernel_radius }} * {{ kernel_radius }}; i++) {
                row = (i / {{ kernel_radius }} - center) * ResY;
                col = i % {{ kernel_radius }} - center;
        
                //can take values from local mem or not
                if (threadIdx.y + row > 0 && threadIdx.y + row < blockDim.y - 1 && threadIdx.x + col > 0 && threadIdx.x + col < blockDim.x - 1) {
                    s += luminance[local_block_pos + row + col] * GKernel[i];
                }
                else {
                    s += output[index + row * 3 + col * 3] * GKernel[i];
                }
            }
        
            // add result to each of RGB pixels
            for (int i = 0; i < 3; i++) {
                image[index + i] = min(image[index + i] + s, 255.0);
            }
        }
    """
    # initialize CUDA runtime
    cuda.init()

    # select the first available GPU device
    device = cuda.Device(0)

    # create the CUDA context of the device
    device.make_context()

    # generate gaussian kernel
    gaussian_kernel = get_gaussian_kernel(kernel_radius, sigma)

    # creating a jinja2 Python template from the kernel code
    tpl = Template(kernel_code)

    # substituting the concrete kernel_radius into the template
    rendered_tpl = tpl.render(kernel_radius=kernel_radius)

    # compiling the CUDA code
    mod = SourceModule(rendered_tpl)

    # extracting the reference to kernel function
    func = mod.get_function("calc_luminance")

    # defining grid
    block_size = 32
    block = (block_size, block_size, 1)
    grid_x = math.ceil(image.shape[0] / block_size)
    grid_y = math.ceil(image.shape[1] / block_size)

    grid = (grid_x, grid_y, 1)

    # allocating memory and transfer data
    max_lum = np.zeros((1, 1), dtype=int)

    image_gpu = cuda.mem_alloc(image.nbytes)
    output_gpu = cuda.mem_alloc(image.nbytes)
    kernel_gpu = cuda.mem_alloc(gaussian_kernel.nbytes)
    max_lum_gpu = cuda.mem_alloc(max_lum.nbytes)

    cuda.memcpy_htod(image_gpu, image)
    cuda.memcpy_htod(kernel_gpu, gaussian_kernel)
    cuda.memcpy_htod(max_lum_gpu, max_lum)

    # calculate luminances
    func(
        image_gpu,
        output_gpu,
        np.int32(image.shape[0]),
        np.int32(image.shape[1]),
        max_lum_gpu,
        block=block,
        grid=grid
    )
    cuda.Context.synchronize()

    # allocating memory on host for results
    intermediate_gpu = np.zeros_like(image)

    cuda.memcpy_dtoh(intermediate_gpu, output_gpu)
    cuda.memcpy_dtoh(max_lum, max_lum_gpu)

    # extracting the reference to kernel function
    func = mod.get_function("bloom_inner")

    # bloom inner block pixels
    func(
        image_gpu,
        output_gpu,
        np.int32(image.shape[0]),
        np.int32(image.shape[1]),
        kernel_gpu,
        np.int32(max_lum[0][0] * threshold),
        block=block,
        grid=grid,
        shared=block_size * block_size * 8
    )
    cuda.Context.synchronize()

    # allocating memory on host for results
    image_bloomed_gpu = np.zeros_like(image)
    cuda.memcpy_dtoh(image_bloomed_gpu, image_gpu)
    cuda.memcpy_dtoh(intermediate_gpu, output_gpu)

    # extracting the reference to kernel function
    func = mod.get_function("bloom_boundary")

    # bloom boundary block pixels
    func(
        image_gpu,
        output_gpu,
        np.int32(image.shape[0]),
        np.int32(image.shape[1]),
        kernel_gpu,
        block=block,
        grid=grid,
        shared=block_size * block_size * 8
    )
    cuda.Context.synchronize()

    # allocating memory on host for results
    cuda.memcpy_dtoh(image_bloomed_gpu, image_gpu)

    # free resources
    cuda.Context.pop()

    return image_bloomed_gpu


def cpu_bloom(image, threshold, kernel_radius, sigma):
    # generate gaussian kernel
    gaussian_kernel = get_gaussian_kernel(kernel_radius, sigma)

    # init luminances
    luminance = np.zeros((image.shape[0], image.shape[1]))

    # calc luminances
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):
            luminance[i, j] = image[i, j, 0] * 0.21 + image[i, j, 1] * 0.72 + image[i, j, 2] * 0.07

    # luminance thresholding
    m = np.amax(luminance)
    m = m * threshold
    luminance = np.multiply(luminance, luminance > m)

    # bloom
    pad = int((kernel_radius - 1) / 2)
    for i in range(0, image.shape[0]):
        for j in range(0, image.shape[1]):

            # gaussian filtering coefficient
            s = 0
            for x in range(-pad, pad + 1):
                for y in range(-pad, pad + 1):
                    if luminance.shape[0] > i + x > 0 and luminance.shape[1] > j + y > 0:
                        s += luminance[i + x, j + y] * gaussian_kernel[x + pad][y + pad]

            # adding filtering coefficient
            image[i, j, 0] = min(image[i, j, 0] + s, 255)
            image[i, j, 1] = min(image[i, j, 1] + s, 255)
            image[i, j, 2] = min(image[i, j, 2] + s, 255)

    return image


def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--image",
                        required=True,
                        help="image to be bloomed")
    parser.add_argument("-o", "--output",
                        dest="name",
                        required=False,
                        help="name for output file with no extension")
    parser.add_argument("-t", "--threshold",
                        type=float,
                        default=0.5,
                        required=False,
                        help="threshold ratio of max luminance to be left")
    parser.add_argument("-k", "--kernel",
                        type=int,
                        default=7,
                        required=False,
                        help="Gaussian kernel radius")
    parser.add_argument("-d", "--deviation",
                        type=float,
                        default=1.0,
                        required=False,
                        help="Gaussian kernel std")
    parser.add_argument("--cpu",
                        dest="cpu",
                        choices=['y', 'n'],
                        default='n',
                        required=False,
                        help="enable cpu task")

    args = parser.parse_args()

    try:
        # loading image
        image = io.imread(args.image).astype('float32')

        [image_name, image_ext] = os.path.splitext(args.image)

        # bloom with gpu
        gpu_start = perf_counter()
        gpu_image = gpu_bloom(image, args.threshold, args.kernel, args.deviation)
        gpu_end = perf_counter()

        # save gpu result
        io.imsave(image_name + '_bloomed_gpu' + image_ext, gpu_image.astype('uint8'), check_contrast=False)

        if args.cpu == 'y':
            # bloom with cpu
            cpu_start = perf_counter()
            cpu_image = cpu_bloom(image, args.threshold, args.kernel, args.deviation)
            cpu_end = perf_counter()

            # save cpu result
            io.imsave(image_name + '_bloomed_cpu' + image_ext, cpu_image.astype('uint8'), check_contrast=False)

        print('Total time (milliseconds) GPU = ', (gpu_end - gpu_start) * 1000)

        if args.cpu == 'y':
            print('Total time (milliseconds) CPU = ', (cpu_end - cpu_start) * 1000)

        return 0
    except:
        print('Sadly, some error occurred(')
        raise


if __name__ == '__main__':
    main()
